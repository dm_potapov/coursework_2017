﻿using SmartTreasury.Classes.Models;
using SmartTreasury.Classes.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.BusinessLogic
{
    /// <summary>
    /// Класс-прослойка для работы с хранилищем платёжных обязательств
    /// </summary>
    public class PaymentsService
    {
        /// <summary>
        /// Репозиторий платёжных обязательств
        /// </summary>
        private IRepository<Payment> _repository;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository"></param>
        public PaymentsService(IRepository<Payment> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Возвращает все платёжные обязательства
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Payment> Get()
        {
            return _repository.ToList();
        }

        /// <summary>
        /// Возвращает платёжное обязательство по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Payment GetById(Guid id)
        {
            return _repository.GetById(id);
        }

        /// <summary>
        /// Сохраняет платёжное обязательство в хранилище
        /// </summary>
        /// <param name="payment"></param>
        public void Put(Payment payment)
        {
            _repository.Save(payment);
        }

        /// <summary>
        /// Удаляет платёжное обязательство по id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(Guid id)
        {
            var elem = _repository.FirstOrDefault(el => el.Id == id);
            if (elem != null)
            {
                _repository.Delete(elem);
            }
        }

    }
}
