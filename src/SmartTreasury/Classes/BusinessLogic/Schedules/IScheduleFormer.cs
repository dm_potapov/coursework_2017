﻿using SmartTreasury.Classes.Models.Schedules;

namespace SmartTreasury.Classes.BusinessLogic
{
    public interface IScheduleFormer
    {
        Schedule FormSchedule();
    }
}