﻿using SmartTreasury.Classes.Models;
using SmartTreasury.Classes.Models.Schedules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.BusinessLogic.Schedules
{
    /// <summary>
    /// Фабрика объектов - частей расписания (ScheduleItem)
    /// </summary>
    public class ScheduleItemFactory
    {
        /// <summary>
        /// Создаёт объект класса ScheduleItem
        /// </summary>
        /// <param name="paymentPart">Платёж</param>
        /// <param name="paymentId">ID платёжного обязательства</param>
        /// <returns></returns>
        public ScheduleItem GetFromPaymentPart(PaymentPart paymentPart, Guid paymentId)
        {
            return new ScheduleItem()
            {
                Date = paymentPart.Date,
                Sum = paymentPart.Sum,
                PaymentId = paymentId
            };
        }
    }
}
