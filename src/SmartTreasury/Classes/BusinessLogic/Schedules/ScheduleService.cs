﻿using SmartTreasury.Classes.BusinessLogic.Schedules;
using SmartTreasury.Classes.Models;
using SmartTreasury.Classes.Models.Schedules;
using SmartTreasury.Classes.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.BusinessLogic
{
    /// <summary>
    /// Класс-прослойка для работы с хранилищем графиков платежей
    /// </summary>
    public class ScheduleService
    {
        /// <summary>
        /// Репозиторий графиков платежей
        /// </summary>
        private IRepository<Schedule> _repository;

        /// <summary>
        /// Объект класса, формирующего графики платежей
        /// </summary>
        private IScheduleFormer _former;

        private PaymentsService _paymentsService;

        private ScheduleItemDTOCollectionFactory _collectionFactory;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="former"></param>
        public ScheduleService(IRepository<Schedule> repository, IScheduleFormer former,
                                PaymentsService paymentsService,
                                ScheduleItemDTOCollectionFactory collectionFactory)
        {
            _repository = repository;
            _former = former;
            _paymentsService = paymentsService;
            _collectionFactory = collectionFactory;
        }

        /// <summary>
        /// Возвращает список частей последнего составленного графика платежей
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ScheduleItemDTO> GetScheduleItemsFromLatest()
        {
            var max = _repository.Max(vv => vv.CreateDate);
            Schedule latest = _repository.Single(v => v.CreateDate == max);
            IEnumerable<Payment> payments = _paymentsService.Get();
            return _collectionFactory.FromScheduleItemsListAndPaymentsList(latest.ScheduleItems, payments);
        }

        public IEnumerable<ScheduleItemDTO> GetFromLatestByDates(DateTime from, DateTime to)
        {
            var res = GetScheduleItemsFromLatest();
            if (from != null)
            {
                res = res.Where(v => v.Date >= from);
            }
            if (to != null)
            {
                res = res.Where(v => v.Date <= to);
            }
            return res;
        }

        /// <summary>
        /// Формирует новый график и сохраняет его в хранилище
        /// </summary>
        public void AddSchedule()
        {
            Schedule s = _former.FormSchedule();
            _repository.Save(s);
        }

        /// <summary>
        /// Удаляет график по id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(Guid id)
        {
            var elem = _repository.FirstOrDefault(el => el.Id == id);
            if (elem != null)
            {
                _repository.Delete(elem);
            }
        }

    }
}
