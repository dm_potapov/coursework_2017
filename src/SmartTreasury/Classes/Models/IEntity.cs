﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.Models
{
    /// <summary>
    /// Интерфейс для сущностей, хранящихся в репозиториях 
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Id сущности
        /// </summary>
        Guid Id { get; set; }
    }
}
