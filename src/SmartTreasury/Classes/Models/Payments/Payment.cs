﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.Models
{
    /// <summary>
    /// Класс-представление платёжного обязательства (договор, кредит и т.д.)
    /// </summary>
    public class Payment : IEntity
    {
        /// <summary>
        /// ID платёжного обязательства
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название платёжного обязательства
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Тип платёжного обязательства
        /// </summary>
        public PaymentType PType { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Сумма платёжного обязательства
        /// </summary>
        public decimal Sum { get; set; }

        /// <summary>
        /// Сумма, оставшаяся к оплате
        /// </summary>
        public decimal SumRemained { get; set; }

        /// <summary>
        /// Список платежей по данному платёжному обязательству
        /// </summary>
        public IEnumerable<PaymentPart> PaymentParts { get; set; }

    }
}
