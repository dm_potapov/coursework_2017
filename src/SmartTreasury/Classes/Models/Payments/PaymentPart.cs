﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.Models
{
    /// <summary>
    /// Класс-представление платежа по заданному платёжному обязательству
    /// </summary>
    public class PaymentPart
    {
        /// <summary>
        /// Крайний срок, к которому должен быть произведён платёж
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма данного платежа
        /// </summary>
        public decimal Sum { get; set; }
    }
}
