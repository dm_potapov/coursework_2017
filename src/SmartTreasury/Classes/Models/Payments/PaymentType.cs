﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.Models
{
    /// <summary>
    /// Перечисление типов платёжных обязательств
    /// </summary>
    public enum PaymentType
    {
        /// <summary>
        /// Единоразовая оплата
        /// </summary>
        OneTime,

        /// <summary>
        /// Ежемесячная оплата
        /// </summary>
        Monthly,

        /// <summary>
        /// Оплата по графику
        /// </summary>
        Custom
    }
}
