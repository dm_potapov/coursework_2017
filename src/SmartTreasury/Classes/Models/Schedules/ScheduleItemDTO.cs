﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.Models.Schedules
{
    /// <summary>
    /// Класс, представляющий часть графика платежей и предназначающийся для отправки клиенту
    /// </summary>
    public class ScheduleItemDTO
    {
        /// <summary>
        /// Id платёжного обязательства
        /// </summary>
        public Guid PaymentId { get; set; }

        /// <summary>
        /// Имя платёжного обязательства
        /// </summary>
        public string PaymentName { get; set; }

        /// <summary>
        /// Дата платежа по графику
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal Sum { get; set; }
    }
}
