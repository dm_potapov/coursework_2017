﻿using MongoDB.Driver;
using SmartTreasury.Classes.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SmartTreasury.Classes.Storage
{
    /// <summary>
    /// Реализация репозитория для БД MongoDB
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class MongoDbRepository<TEntity> : IRepository<TEntity> where TEntity : IEntity
    {
        /// <summary>
        /// Представление коллекции MongoDB
        /// </summary>
        private IMongoCollection<TEntity> _collection;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="client">Объект для доступа к БД MongoDB</param>
        /// <param name="dbName">Название БД</param>
        /// <param name="collectionName">(опционально) Название коллекции</param>
        public MongoDbRepository(MongoClient client, string dbName, string collectionName = null)
        {
            var db = client.GetDatabase(dbName);
            if (collectionName == null)
            {
                collectionName = $"{typeof(TEntity).Name}_col";
            }
            _collection = db.GetCollection<TEntity>(collectionName);
        }

        public Type ElementType
        {
            get
            {
                return _collection.AsQueryable().ElementType;
            }
        }

        public Expression Expression
        {
            get
            {
                return _collection.AsQueryable().Expression;
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                return _collection.AsQueryable().Provider;
            }
        }

        /// <summary>
        /// Возвращает элемент из репозитория по его ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TEntity GetById(Guid id)
        {
            return _collection.Find(elem => elem.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Обновляет элемент, либо добавляет новый, если искомого элемента нет
        /// </summary>
        /// <param name="T"></param>
        public void Save(TEntity t)
        {
            var elem = GetById(t.Id);
            if (elem == null)
            {
                _collection.InsertOne(t);
            } 
            else
            {
                _collection.ReplaceOne(el => el.Id == t.Id, t);
            }
        }

        /// <summary>
        /// Удаляет элемент
        /// </summary>
        /// <param name="T"></param>
        public void Delete(TEntity t)
        {
            _collection.DeleteOne(elem => elem.Id == t.Id);
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return _collection.AsQueryable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _collection.AsQueryable().GetEnumerator();
        }
    }
}
