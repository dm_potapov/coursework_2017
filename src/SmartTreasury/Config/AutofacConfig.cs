﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using SmartTreasury.Classes.BusinessLogic;
using SmartTreasury.Classes.BusinessLogic.Payments;
using SmartTreasury.Classes.BusinessLogic.Schedules;
using SmartTreasury.Classes.Models;
using SmartTreasury.Classes.Models.Schedules;
using SmartTreasury.Classes.Storage;

namespace SmartTreasury.Config
{
    public static class AutofacConfig
    {
        public static ContainerBuilder Config(IConfigurationRoot config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<MongoClient>().AsSelf()
                        .WithParameter("connectionString", config.GetValue<string>("Database:Host"))
                        .InstancePerLifetimeScope();

            string dbName = config.GetValue<string>("Database:Db");
            builder.RegisterType<MongoDbRepository<Payment>>().As<IRepository<Payment>>()
                            .WithParameter("dbName", dbName);
            builder.RegisterType<MongoDbRepository<Schedule>>().As<IRepository<Schedule>>()
                            .WithParameter("dbName", dbName);

            builder.RegisterType<PaymentsService>().AsSelf();
            builder.RegisterType<ScheduleService>().AsSelf();

            builder.RegisterType<MonthlyPaymentPartsMaker>().AsSelf();
            builder.RegisterType<ScheduleItemDTOCollectionFactory>().AsSelf();
            builder.RegisterType<ScheduleItemFactory>().AsSelf();
            builder.RegisterType<ScheduleFormer>().As<IScheduleFormer>();

            return builder;
        }
    }
}
