﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SmartTreasury.Classes.Models;
using SmartTreasury.Classes.BusinessLogic;
using SmartTreasury.Classes.Storage;

namespace SmartTreasury.Controllers
{
    /// <summary>
    /// API-контроллер для работы с платёжными обязательствами
    /// </summary>
    [Route("api/[controller]")]
    public class PaymentsController : Controller
    {
        private PaymentsService _service;

        public PaymentsController(PaymentsService service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<Payment> Get()
        {
            return _service.Get();
        }

        [HttpGet("{id:guid}")]
        public Payment Get(Guid id)
        {
            return _service.GetById(id);
        }

        [HttpPost]
        public void Post([FromBody]Payment payment)
        {
            _service.Put(payment);
        }

        [HttpDelete("{id:guid}")]
        public void Delete(Guid id)
        {
            _service.Delete(id);
        }
    }
}
