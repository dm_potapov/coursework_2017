﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SmartTreasury.Classes.Models.Schedules;
using SmartTreasury.Classes.BusinessLogic;


namespace SmartTreasury.Controllers
{
    /// <summary>
    /// API-контроллер для работы с графиками платежей
    /// </summary>
    [Route("api/[controller]")]
    public class ScheduleController : Controller
    {
        private ScheduleService _service;

        public ScheduleController(ScheduleService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("latest")]
        public IEnumerable<ScheduleItemDTO> GetScheduleItemsFromLatest()
        {
            return _service.GetScheduleItemsFromLatest();
        }
        
        [HttpGet]
        [Route("latest/{from:datetime}/{to:datetime}")]
        public IEnumerable<ScheduleItemDTO> GetFromLatestByDates(DateTime from, DateTime to)
        {
            return _service.GetFromLatestByDates(from, to);
        }

        [HttpPost]
        public IEnumerable<ScheduleItemDTO> Put()
        {
            _service.AddSchedule();
            return _service.GetScheduleItemsFromLatest();
        }
    }
}
