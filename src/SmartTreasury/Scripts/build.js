﻿var angular = require('angular');
//require('ui-router-core');
require('angular-ui-router');
require('angular-animate');
require('angular-sanitize');
require('angular-ui-bootstrap');

require('./main');

require('./services/payment_validation_service');
require('./services/date_transform_service');
require('./services/alert_service');
require('./services/payments_service');
require('./services/schedule_service');

require('./components/main');
require('./components/payments');
require('./components/payment');
require('./components/schedule');