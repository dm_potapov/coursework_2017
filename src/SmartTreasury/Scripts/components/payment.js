﻿angular.module('smartTreasury')
.component('payment', {
    bindings: { payment: '=' },
    templateUrl: 'templates/payment_form.html',
    controller: function ($state, PaymentsService, AlertService) {
        $ctrl = this;
        $ctrl.pTypes = [
            'Единоразовый',
            'Ежемесячный',
            'По графику',
        ];
        $ctrl.savePayment = function () {
            console.log($ctrl.form);
            PaymentsService.PutPayment($ctrl.payment);
        };
        $ctrl.addPaymentPart = function () {
            $ctrl.payment.paymentParts.push({
                "date": new Date(),
                "sum": 0
            })
            $ctrl.sortByDate();
        }
        $ctrl.sortByDate = function () {
            $ctrl.payment.paymentParts.sort(function (a, b) {
                return (a.date > b.date) ? 1 : (a.date < b.date) ? -1 : 0;
            });
        }
        $ctrl.removePaymentPart = function (index) {
            if ($ctrl.payment.paymentParts.length > 1) {
                $ctrl.payment.paymentParts.splice(index, 1);
            }
        }
    }
})