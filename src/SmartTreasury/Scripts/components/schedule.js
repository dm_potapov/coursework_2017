﻿angular.module('smartTreasury')
.component('schedule', {
    bindings: { schedule: '=' },
    templateUrl: "templates/schedule.html",
    controller: function (ScheduleService) {
        $ctrl = this;
        $ctrl.notificationOpened = true;
        $ctrl.ReformSchedule = function () {
            ScheduleService.FormSchedule($ctrl);
            $ctrl.clearDates();
        }
        $ctrl.clearDates = function () {
            $ctrl.dateFrom = "";
            $ctrl.dateTo = "";
        }
        $ctrl.FilterByDate = function () {
            ScheduleService.GetScheduleByDates($ctrl, $ctrl.dateFrom, $ctrl.dateTo);
        }
    }
})