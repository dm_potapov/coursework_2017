﻿window.onAjaxError = function (response) {
    alert(response.data);
}

var app = angular.module("smartTreasury", ['ui.router', 'ngAnimate', 'ui.bootstrap']);

app.config(function ($stateProvider) {
    var states = [
      {
          name: 'main',
          url: '/',
          component: 'main'
      },

      {
          name: 'payments',
          url: '/payments',
          component: 'payments',
          resolve: {
              payments: function (PaymentsService) {
                  return PaymentsService.GetPayments();
              }
          }
      },
      {
          name: 'payment',
          url: '/payments/{id}',
          component: 'payment',
          resolve: {
              payment: function (PaymentsService, $transition$) {
                  return PaymentsService.GetPayment($transition$.params().id);
              }
          }
      },

      {
          name: 'schedule',
          url: '/schedule',
          component: 'schedule',
          resolve: {
              schedule: function (ScheduleService) {
                  return ScheduleService.GetSchedule();
              }
          }
      }
    ]

    states.forEach(function (state) {
        $stateProvider.state(state);
    });
});