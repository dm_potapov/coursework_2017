﻿angular.module('smartTreasury')
.service('PaymentsService', function ($state, $http, 
                                    DateTransformService, AlertService, PaymentValidationService) {
    return {
        GetPayments: function () {
            return $http.get('api/payments/').then(function (resp) {
                DateTransformService.transformMultipleDates(resp.data, "createDate", "d-m-y");
                return resp.data;
            }, window.onAjaxError);
        },
     
        GetPayment: function (id) {
            if (id == 0) {
                return { paymentParts: [{ date: null, sum: 0 }] };
            }
            else {
                return $http.get('api/payments/' + id).then(function (resp) {
                    resp.data.createDate = new Date(resp.data.createDate);
                    for (i in resp.data.paymentParts) {
                        resp.data.paymentParts[i].date = new Date(resp.data.paymentParts[i].date);
                    }
                    return resp.data;
                }, window.onAjaxError);
            }
        },

        PutPayment: function (payment) {
            var validationResult = PaymentValidationService.validate(payment);
            if (validationResult == 'ok')
            {
                if (payment.pType !== 2) {
                    payment.paymentParts = [payment.paymentParts.pop()];
                    payment.paymentParts[0].sum = payment.sum;
                }
                return $http.post('api/payments/', payment).then(function (resp) {
                    AlertService.showMessage('success', 'Успешно добавлено/отредактировано', 5000);
                    $state.go('payments');
                }, window.onAjaxError);
            }
            else {
                AlertService.showMessage('danger', validationResult, 5000);
            }
        },

        RemovePayment: function (guid) {
            return $http.delete('api/payments/' + guid).then(function (resp) {
                AlertService.showMessage('success', 'Успешно удалено', 5000);
            }, window.onAjaxError);
        }
    }
});