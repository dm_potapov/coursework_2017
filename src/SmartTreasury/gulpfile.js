﻿/// <binding Clean='clean' />

var gulp = require('gulp');
var del = require('del');
var webpack = require('webpack-stream');

gulp.task('clean', function () {
    return del('wwwroot/scripts/**/*');
});

gulp.task('default', function () {
    return gulp.src('scripts/build.js')
      .pipe(webpack({
          output: {
              filename: 'bundle.js'
          }
      }))
      .pipe(gulp.dest('wwwroot/scripts/'));
});

gulp.task('watch', function () {
    return gulp.watch(['scripts/**/*'], ['default']);
});